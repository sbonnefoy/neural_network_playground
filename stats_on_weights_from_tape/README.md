# Stats on weights
A juypter notebook to get some statistics on the model weights after training. 

In the notebook you can: 
- Create a model from scratch
- Train it using the gradient tape
- Check the weights distribution
- Check the gradient distribution
- Check the relative gradient distribution w.r.t to the weights
